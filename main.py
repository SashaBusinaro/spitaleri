#!.venv/bin/python
import sqlite3
from datetime import datetime
import os.path
import hashlib
from codicefiscale import codicefiscale

#Ottieni la data di oggi
date = datetime.today().strftime('%Y-%m-%d')

#Imposta il nome del file database
dbname = "dsan"+".db"

#Funzine connect al database
def connect():
  conn = sqlite3.connect(dbname)
  cursor = conn.cursor()
  cur = cursor.execute
  return conn,cursor,cur

#Funzione per la creazione di un nuovo utente
def new_user(fcode):
  
  input_nome = str(input("Nome: "))
  input_cognome = str(input("Cognome: "))
  input_birthdate = str(input("Data di nascita (YYYY-MM-DD): "))
  input_sex = str(input("Sesso [M/F]: "))
  input_birthplace = str(input("Luogo di nascita: "))
  input_fcode = fcode
  input_password = input("Imposta una password: ")
  if len(input_password) < 3:
    print("password troppo corta, riprovare con almeno 3 caratteri ")
    exit()
  #Encrypt the password
  input_password = str(hashlib.sha256((input_password).encode("utf-8")).hexdigest())
  #Check the fcode
  fcode_check = codicefiscale.encode(surname=input_cognome, name=input_nome, sex=input_sex, birthdate=input_birthdate, birthplace=input_birthplace)

  if not fcode_check==input_fcode:
     print("Autenticazione fallita, riprovare...")
     exit()
  else:
    print("Utente verificato con successo")
    print("\n")
  
  return input_nome, input_cognome, input_birthdate, input_sex, input_birthplace, input_fcode, input_password

#Funzione per aggiungere i sintomi ad un utente
def add_sintomi(recov_id):
  #user input
  print("Prego compilare i seguenti campi...")
  inputS1 = str(input("Oggi presenta febbre?: "))
  inputS2 = str(input("Oggi presenta raffreddore?: "))
  inputS3 = str(input("Oggi presenta cefalea?: "))
 
  #add the given data
  
  cur('''INSERT INTO sintomi 
  (id,date,febbre,raffreddore,cefalea)
  VALUES
    ( "'''+recov_id+'''", "'''+date+'''", "'''+inputS1+'''", "'''+inputS2+'''", "'''+inputS3+'''");
  '''),

  conn.commit()
  print('Dati salvati con successo')

  cur("SELECT nome, cognome FROM utenti WHERE ID is '"+str(recov_id)+"'")
  print("Utente: ")
  record = cursor.fetchall()
  for item in record:
    print(item[0],item[1])
  
  cur("SELECT date, febbre, raffreddore, cefalea FROM sintomi WHERE ID is '"+str(recov_id)+"'")
  print("\nStorico dei dati inseriti: ")
  names = [description[0] for description in cursor.description]
  rows = cursor.fetchall()
  for row in rows:
    print("\n")
    for name, val in zip(names,row):
      print( name, val)

  return inputS1, inputS2, inputS3

#Se il file database non esiste, creane uno e inizializzalo
if not os.path.exists('./'+dbname):
  
  #Creazione del primo utente con la funzione new_user()
  fcode = str(input("Creazione primo utente, inserire il codice fiscale: "))
  input_nome, input_cognome, input_birthdate, input_sex, input_birthplace, input_fcode, input_password = new_user(fcode)
  
  conn, cursor, cur = connect()

  #default data init
  cur('''CREATE TABLE IF NOT EXISTS utenti 
  (
    ID integer,
    nome text,
    cognome text,
    birthdate datetime,
    sex text,
    birthplace text,
    fcode text,
    password text
  );'''),
  cur('''CREATE TABLE IF NOT EXISTS sintomi 
  (
    ID integer,
    date string,
    febbre integer,
    raffreddore integer,
    cefalea integer
  );'''),


  cur('''INSERT INTO utenti 
    (id, nome, cognome, birthdate, sex, birthplace, fcode, password)
    VALUES
      (1, ?, ?, ?, ?, ?, ?, ?);
    ''',(input_nome, input_cognome, input_birthdate, input_sex, input_birthplace, fcode, input_password)),

  recov_id = '1'
  add_sintomi(recov_id)

  conn.commit()

  print("Inizializzazione completata")

conn, cursor, cur = connect()


#Funzione che permette il login degli utenti
def login():
  fcode = str((input("Login utente, inserire il codice fiscale: ")))
  #Ottieni tutti i codici fiscali nella tabella utenti  
  cur("SELECT fcode FROM utenti")
  fcode_tuple = cursor.fetchall()
  #print("fcode presenti: ",fcode_tuple)
  #Controlla se il fcode inserito esiste nella tabella
  for elem in fcode_tuple:
    if fcode in elem:
      #Se esiste, ottieni il relativo id utente
      cur("SELECT id FROM utenti WHERE fcode is '"+str(fcode)+"'")
      recov_id = str(cursor.fetchone()[-1])
      print("ID Utente: ",recov_id)

      # password check
      input_pass = str(hashlib.sha256(((input("inserire la password: "))).encode("utf-8")).hexdigest())
      cur("SELECT password FROM utenti WHERE id is '"+str(recov_id)+"'")
      password = (cursor.fetchone()[-1])
      if input_pass == password:
        print('Accesso eseguito')
      else:
        print("Password sbagliata, riprovare...")
        exit()

      print("Data di oggi: "+date)

      # select the last tabel date
      cur("SELECT * FROM sintomi")
      cursor.execute("SELECT date FROM sintomi WHERE id is '"+str(recov_id)+"'")
      #Controllo della data dell'ultimo dato inserito
      datecheck=cursor.fetchall()[-1]
      if datecheck[-1]==date:
        print("dati già inseriti oggi")
        exit()
      else:
        add_sintomi(recov_id)
        cur("SELECT * FROM sintomi")
        print("\n")
        exit()
  else:
    #Ottenimento di un nuovo id utente univoco
    cur("SELECT ID FROM utenti")
    last_id = cursor.fetchall()[-1]
    new_id=last_id[-1]+1
    print("ID utente: ",new_id)

    #Creazione del nuovo utente
    input_nome, input_cognome, input_birthdate, input_sex, input_birthplace, input_fcode, input_password = new_user(fcode)
    
    cur('''INSERT INTO utenti 
    (id, nome, cognome, birthdate, sex, birthplace, fcode, password)
    VALUES
      (?, ?, ?, ?, ?, ?, ?, ?);
    ''',(str(new_id), input_nome, input_cognome, input_birthdate, input_sex, input_birthplace, fcode, input_password)),

    conn.commit()

    recov_id = str(new_id)
    #Inserimento dei sintomi per il nuovo utente
    add_sintomi(recov_id)
  return fcode, fcode_tuple, last_id, new_id, recov_id
fcode, fcode_tuple, last_id, new_id, recov_id = login()






cursor.close()
conn.commit()
conn.close()